import * as React from 'react';
import { objectInputTypes, dataTypes } from '../../../utils/constants/enums';
import NumberFilter from '../numberFilter/NumberFilter';
import BooleanFilter from '../booleanFilter/BooleanFilter';
import DateFilter from '../dateFilter/DateFilter';
import { HelperMethods } from '../../../utils/helperMethods/HelperMethods';

export interface IProps {
    filter: any;
    onChange: Function;
    searchableProperties: any;
}
 
export interface IState {
}

export default class ObjectFilter extends React.Component<IProps, IState> {
    private getSearchableProperties(searchableProp) {
        const object = "object";
        if (typeof searchableProp === object) {
            return object;
        }
        return searchableProp;
    }

    onFilterOptionChange = (inputType: objectInputTypes, inputValue: string) => {
        const { filter, onChange, searchableProperties } = this.props;
        let value;
        if (filter && filter.value) {
            value = filter.value;
        } else {
            value = {};
        }

        value[inputType] = { propName: inputValue, propType: this.getSearchableProperties(searchableProperties[inputValue]) };
        onChange(value);
    }

    renderObjectOptions(searchableProperties: any) {
        return Object.keys(searchableProperties).map(k => <option key={k} value={k}>{k}</option>);
    }

    renderAppropriateFilter(filter: any, onChange: Function, searchableProperties: any) {
        if (filter && filter.value) {
            const dataType = filter.value[objectInputTypes.select].propType;
            switch (dataType) {
                case dataTypes.number:
                    if (!filter.value.numberFilter) {
                        filter.value.numberFilter = {};
                    }
                    return <NumberFilter filter={filter.value.numberFilter} onChange={value => { filter.value.numberFilter.value = value; onChange(filter.value)}} />;
            
                case dataTypes.string:
                    if (!filter.value.stringFilter) {
                        filter.value.stringFilter = {};
                    }
                    return <input onChange={event => { filter.value.stringFilter.value = event.target.value; onChange(filter.value)}} />;
                
                case dataTypes.boolean:
                    if (!filter.value.booleanFilter) {
                        filter.value.booleanFilter = {};
                    }
                    return <BooleanFilter filter={filter.value.booleanFilter} onChange={value => { filter.value.booleanFilter.value = value; onChange(filter.value)}} />;
                
                case dataTypes.date:
                    if (!filter.value.dateFilter) {
                        filter.value.dateFilter = {};
                    }
                    return <DateFilter filter={filter.value.dateFilter} onChange={value => { filter.value.dateFilter.value = value; onChange(filter.value)}} />;

                case dataTypes.object:
                    const { readProp } = HelperMethods;
                    if (!filter.value.objectFilter) {
                        filter.value.objectFilter = {};
                    }

                    const sPAcessor = filter.value.select.propName;
                    const searchableProps = readProp(searchableProperties, sPAcessor) ;
                    return <ObjectFilter filter={filter.value.objectFilter} onChange={value => { filter.value.objectFilter.value = value; onChange(filter.value)}} searchableProperties={searchableProps} />;

                default:
                    break;
            }
        }
        return null;
    }

    render() {
        const { filter, searchableProperties, onChange } = this.props;
        return (
            <div style={{ display: 'flex', flexDirection: 'column' }}>
                <select
                    onChange={event => this.onFilterOptionChange(objectInputTypes.select, event.target.value)}
                    style={{ width: "100%" }}
                    value={filter && filter.value ? filter.value[objectInputTypes.select].propName : "all"}
                >
                    <option value='all'>Show All</option>
                    {this.renderObjectOptions(searchableProperties)}
                </select>
                {this.renderAppropriateFilter(filter, onChange, searchableProperties)}
            </div>
        );
    }
}