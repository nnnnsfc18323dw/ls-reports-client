import * as React from 'react'

export interface IProps {
	filter: any;
	onChange: Function;
}

export default class BooleanFilter extends React.Component<IProps, {}> {
	render() {
		const { filter, onChange } = this.props;
		return (
			<select
                onChange={event => onChange(event.target.value)}
                style={{ width: "100%" }}
                value={filter ? filter.value : "all"}
            >
                <option value="All">Show All</option>
                <option value="True">True</option>
                <option value="False">False</option>
            </select>
		)
	}
}
