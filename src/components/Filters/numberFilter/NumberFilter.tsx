import * as React from 'react';
import { numberInputTypes, numberFilterModes } from '../../../utils/constants/enums';

export interface IProps {
    filter: any;
    onChange: Function;
}
 
export interface IState {
}

export default class NumberFilter extends React.Component<IProps, IState> {
    // organize the 3 inputs to one object that will be passed as filter value in across this column
    private onNumberChange = (inputType: numberInputTypes, inputValue: any) => {
        const { filter, onChange } = this.props;
        let value;
        if (filter && filter.value) {
            value = filter.value;
        } else {
            value = {};
        }

        value[inputType] = inputValue;
        onChange(value);
    }

    renderFirstNumberInput(filter: any) {
        if (filter && filter.value && filter.value[numberInputTypes.select] !== "All") {
            return (
                <input
                    type="number"
                    style={{
                        width: '100%',
                    }}
                    value={filter.value[numberInputTypes.firstNumberInput] || ''}
                    onChange={event => this.onNumberChange(numberInputTypes.firstNumberInput, event.target.value)}
                />
            );
        }
        return null;
    }

    renderSecondNumberInput(filter: any) {
        if (filter && filter.value && filter.value[numberInputTypes.select] === "Between") {
            return (
                <input
                    type="number"
                    style={{
                        width: '100%',
                    }}
                    value={filter.value[numberInputTypes.secondNumberInput] || ''}
                    onChange={event => this.onNumberChange(numberInputTypes.secondNumberInput, event.target.value)}
                />
            );
        }
        return null;
    }

    render() {
        const { filter } = this.props;
        return (
            <div style={{ display: 'flex', flexDirection: 'column' }}>
                <select
                    onChange={event => this.onNumberChange(numberInputTypes.select, event.target.value)}
                    style={{ width: "100%" }}
                    value={filter && filter.value ? filter.value[numberInputTypes.select] : "all"}
                >
                    <option value={numberFilterModes.All}>Show All</option>
                    <option value={numberFilterModes.Equal}>Equal</option>
                    <option value={numberFilterModes.LessThan}>Less than</option>
                    <option value={numberFilterModes.LessThanOrEqual}>Less than or Equal</option>
                    <option value={numberFilterModes.GreaterThan}>Greater than</option>
                    <option value={numberFilterModes.GreaterThanOrEqual}>Greater than or Equal</option>
                    <option value={numberFilterModes.Between}>Between</option>
                </select>
                {this.renderFirstNumberInput(filter)}
                {this.renderSecondNumberInput(filter)}
            </div>
        );
    }
}