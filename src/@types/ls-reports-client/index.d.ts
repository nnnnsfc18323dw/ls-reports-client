import { Component, HTMLAttributes } from 'react';

declare module 'ls-reports-client' {
    export interface IProps {
        baseURL: string;
        reportName: string;
    }
     
    export interface IState {
        columns: LSColumn[];
        data: object[];
        schema: object[];
    }

    export class LSTable extends Component<IProps, IState> {
        exportToExcel(): void;
    }

    class LSColumn {
        
    }
  }