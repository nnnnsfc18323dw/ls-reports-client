import axios from 'axios';

export default class Server {
    static BASE_URL: string;

    static initBaseUrl(baseURL: string) {
        Server.BASE_URL = baseURL;
    }

    get(url) {
        const axios = this.createAxios();
        return axios.get(url);
    }

    createAxios() {
        const config = {
            baseURL: Server.BASE_URL
        };
        return axios.create(config);
    }
}
